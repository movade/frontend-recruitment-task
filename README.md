# Frontend task

## Objective

Single Page application (SPA) which displays a list of cryptocurrency pairings using the public REST API from Bitfinex https://bitfinex.readme.io/v1/reference and  https://bitfinex.readme.io/v2/reference or any one of the major crypto exchanges with an option to display detailed ticker data on a separate page.

## Stack
Following stack should be used in order to achieve the task:

* React (Use create-react-app or any other starter kit to bootstrap your project)
* Redux (react-redux and redux)
* React router or some other clientside router
* HTTP library of any kind (fetch included with create-react-app, axios etc…)
* Test runner such as Jest or Mocha (Jest comes packed with create-react-app). You can use enzyme or react-test-utils for traversing the virtual dom.
* You can use Bootstrap, Rebass, Foundation, Material-UI or any other frontend framework.
* Feel free to use https://github.com/kirillshevch/react-cryptocoins or any other cryptoicon set

## Final objective
* Project starts up correctly and compiles without errors.
* A list of markets by market symbol with icon (eg. 'ETH/USD' or 'Etherium/USD') in a list component is displayed.
* When a list item is clicked a new page with the current ticker details about the pairing being shown with a back button to take the user back to the list. The following fields or equivalents should display on the details page:
  * Asset Icon
  * `BID` or equivalent
  * `BID_SIZE` or equivalent
  * `ASK` or equivalent
  * `ASK_SIZE` or equivalent
  * `DAILY_CHANGE` or equivalent
  * `LAST_PRICE` or equivalent
  * `VOLUME` or equivalent
* Single Market details page should display the Market data after refreshing the page.
* Provide a test for at least 1 react component and 1 reducer.
* Explain your decisions in a short readme. Especially explain any controversial decisions.
* Don't forget to include instructions as to how to use the repo

## Deadline
7 days from receiving the assessment.

Host as a private repository on GitLab and add https://gitlab.com/paula.masiak and https://gitlab.com/mariusz.muraszko as contributors.

## Things we value
- Correct Functionality
- Code quality, formatting and conventions
- Maintainability / Readability
- Component design 
- Git hygiene
- Thorough testing
- Well thought through solution (think before you code)